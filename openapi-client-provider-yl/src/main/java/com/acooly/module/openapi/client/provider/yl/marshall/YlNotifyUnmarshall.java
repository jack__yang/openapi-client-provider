/**
 * create by zhangpu
 * date:2015年3月12日
 */
package com.acooly.module.openapi.client.provider.yl.marshall;


import com.acooly.module.openapi.client.api.exception.ApiClientException;
import com.acooly.module.openapi.client.api.marshal.ApiUnmarshal;
import com.acooly.module.openapi.client.api.message.MessageFactory;
import com.acooly.module.openapi.client.provider.yl.domain.YlNotify;
import com.acooly.module.openapi.client.provider.yl.partner.YlPartnerIdLoadManager;
import com.acooly.module.safety.key.KeyLoadManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

import javax.annotation.Resource;

/**
 * @author
 */
@Service
public class YlNotifyUnmarshall extends YlMarshallSupport
        implements ApiUnmarshal<YlNotify, Map<String, String>> {
    private static final Logger logger = LoggerFactory.getLogger(YlNotifyUnmarshall.class);
    @Resource(name = "ylMessageFactory")
    private MessageFactory messageFactory;

    @Resource(name = "ylPartnerIdLoadManager")
    private YlPartnerIdLoadManager partnerIdLoadManager;

    @Autowired
    private KeyLoadManager keyStoreLoadManager;

    @SuppressWarnings("unchecked")
    @Override
    public YlNotify unmarshal(Map<String, String> message, String serviceName) {
        try {
            logger.info("异步通知{}", message);

            return null;
        } catch (Exception e) {
            throw new ApiClientException("异步通知 解析失败:" + e.getMessage());
        }

    }


}
