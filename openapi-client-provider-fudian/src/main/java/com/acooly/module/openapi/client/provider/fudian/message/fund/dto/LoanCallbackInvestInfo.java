package com.acooly.module.openapi.client.provider.fudian.message.fund.dto;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import java.io.Serializable;

/**
 * @author zhike 2018/3/21 16:24
 */
@Getter
@Setter
public class LoanCallbackInvestInfo implements Serializable {

    /**
     * 本金
     * 金额：元
     */
    @NotBlank
    private String capital;

    /**
     * 利息
     * 金额：元
     */
    @NotBlank
    private String interest;


    /**
     * 利息管理费 金额：元
     */
    @NotBlank
    private String interestFee;


    /**
     * 加息利息 金额：元
     */
    @NotBlank
    private String rateInterest;

    /**
     * 存管账户编号
     * 原有标的，投资人的存管账户账户号
     */
    @NotBlank
    private String investAccountNo;

    /**
     * 原有投资订单日期
     * 原有标的，投资时使用的订单日期
     */
    @NotBlank
    private String investOrderDate;

    /**
     * 原有投资订单号
     * 原有标的，投资时使用的订单号
     */
    @NotBlank
    private String investOrderNo;

    /**
     * 存管账户用户名
     * 原有标的，投资人的存管账户用户名
     */
    @NotBlank
    private String investUserName;

    /**
     * 投资订单日期
     * 当前交易订单日期，格式yyyyMMdd
     */
    @NotBlank
    private String orderDate;

    /**
     * 存管账户用户名
     * 当前交易的订单号，在回款记录中唯一性原有标的，投资人的存管账户用户名
     */
    @NotBlank
    private String orderNo;
}
