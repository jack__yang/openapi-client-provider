/*
 * www.acooly.cn Inc.
 * Copyright (c) 2018 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2018-01-23 12:58 创建
 */
package com.acooly.module.openapi.client.provider.fudian.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

/**
 * @author zhangpu 2018-01-23 12:58
 */
@Getter
@Setter
public class FudianRequest extends FudianMessage {

    /**
     * 商户号
     * 用于校验主体参数和业务参数一致性，保证参数的安全传输
     */
    private String merchantNo;

    @Length(max = 256)
    private String returnUrl;

    /**
     * 异步回调地址
     * 服务器通知业务参数，请根据此做业务处理
     */
    @Length(max = 256)
    private String notifyUrl;


}
