package com.acooly.module.openapi.client.provider.fuyou.marshall;

import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.api.marshal.ApiMarshal;
import com.acooly.module.openapi.client.api.message.PostRedirect;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service
@Slf4j
public class FuyouRedirectPostMarshall extends FuyouMarshallSupport implements ApiMarshal<PostRedirect, FuyouRequest> {

    @Override
    public PostRedirect marshal(FuyouRequest source) {
        PostRedirect postRedirect = new PostRedirect();
        if(Strings.isBlank(source.getPartner())) {
            source.setPartner(getProperties().getPartnerId());
            source.setMchntCd(getProperties().getPartnerId());
        }
        postRedirect.setRedirectUrl(source.getGatewayUrl());
        postRedirect.setFormDatas(doMarshall(source));
        log.info("跳转报文: {}", postRedirect);
        return postRedirect;
    }

    /**
     * 网关支付特殊处理
     * @param source
     * @return
     */
    protected Map<String, String> doMarshall(FuyouRequest source) {
        doVerifyParam(source);
        Map<String, String> requestDataMap = getRequestDataMap(source);
        String signContent = source.getSignStr();
        log.info("待签字符串:{}", signContent);
        source.setSign(doSign(signContent,source.getService()));
        requestDataMap.put("md5", source.getSign());
        log.info("请求报文: {}", requestDataMap);
        return requestDataMap;
    }
}
