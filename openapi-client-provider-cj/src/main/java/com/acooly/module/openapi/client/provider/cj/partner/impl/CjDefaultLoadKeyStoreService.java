package com.acooly.module.openapi.client.provider.cj.partner.impl;

import com.acooly.core.utils.security.RSA;
import com.acooly.module.openapi.client.provider.cj.CjConstants;
import com.acooly.module.openapi.client.provider.cj.CjProperties;
import com.acooly.module.safety.key.AbstractKeyLoadManager;
import com.acooly.module.safety.key.KeyStoreLoader;
import com.acooly.module.safety.support.KeyStoreInfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CjDefaultLoadKeyStoreService extends AbstractKeyLoadManager<KeyStoreInfo> implements KeyStoreLoader {

    @Autowired
    private CjProperties cjShengProperties;

    /**
     * 如果配置文件没有参数，就会去渠道API表去查
     * @param principal
     * @return
     */
    @Override
    public KeyStoreInfo doLoad(String principal) {
        KeyStoreInfo keyStoreInfo = new KeyStoreInfo();
        keyStoreInfo.setCertificateUri(cjShengProperties.getPublicCertPath());
        keyStoreInfo.setKeyStoreUri(cjShengProperties.getPrivateCertPath());
        keyStoreInfo.setKeyStorePassword(cjShengProperties.getPrivateCertPassword());
        keyStoreInfo.setKeyStoreType(RSA.KEY_STORE_PKCS12);
        keyStoreInfo.loadKeys();
        return keyStoreInfo;
    }

    @Override
    public String getProvider() {
        return CjConstants.PROVIDER_NAME;
    }
}
