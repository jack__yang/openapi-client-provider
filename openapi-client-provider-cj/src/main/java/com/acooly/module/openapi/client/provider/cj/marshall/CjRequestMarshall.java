/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu 
 * date:2016年4月1日
 *
 */
package com.acooly.module.openapi.client.provider.cj.marshall;


import com.acooly.module.openapi.client.api.marshal.ApiMarshal;
import com.acooly.module.openapi.client.provider.cj.domain.CjRequest;

import org.springframework.stereotype.Service;

/**
 * @author fufeng
 */
@Service
public class CjRequestMarshall extends CjMarshallSupport implements ApiMarshal<String, CjRequest> {

    @Override
    public String marshal(CjRequest source) {
        beforeMarshall(source);
        return doMarshall(source);
    }

}
