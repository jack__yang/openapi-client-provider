/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhike@acooly.cn 2017-09-17 17:49 创建
 */
package com.acooly.module.openapi.client.provider.baofup;

/**
 * @author zhike 2017-09-17 17:49
 */
public class BaoFuPConstants {
    /**
     * 签名实现KEY和秘钥
     */
    public static final String PROVIDER_NAME = "baofuPProvider";
    public static final String SIGNER_TYPE = "signature";
    public static final String PARTNER_KEY = "member_id";
    public static final String DGTL_ENVLP = "dgtl_envlp";
    public static final String NOTIFY_SUB_URL = "/gateway/notify/baofupNotify/";


    /**
     * 编码
     */
    public final static String ENCODE = "UTF-8";

    public final static String RSA_CHIPER = "RSA/ECB/PKCS1Padding";
    /**
     * 1024bit 加密块 大小
     */
    public final static int ENCRYPT_KEYSIZE = 117;
    /**
     * 1024bit 解密块 大小
     */
    public final static int DECRYPT_KEYSIZE = 128;

}
