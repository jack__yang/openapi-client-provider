package com.acooly.module.openapi.client.provider.jyt;

import com.acooly.module.openapi.client.api.notify.ApiServiceClientServlet;
import com.acooly.module.openapi.client.api.transport.HttpTransport;
import com.acooly.module.openapi.client.api.transport.Transport;
import com.acooly.module.openapi.client.provider.jyt.enums.JytServiceEnum;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import java.util.List;

import static com.acooly.module.openapi.client.provider.jyt.OpenAPIClientJytProperties.PREFIX;

@EnableConfigurationProperties({OpenAPIClientJytProperties.class})
@ConditionalOnProperty(value = PREFIX + ".enable", matchIfMissing = true)
@ComponentScan
public class OpenAPIClientJytConfigration {

    @Autowired
    private OpenAPIClientJytProperties openAPIClientJytProperties;

    @Bean("jytHttpTransport")
    public Transport jytHttpTransport() {
        HttpTransport httpTransport = new HttpTransport();
        httpTransport.setGateway(openAPIClientJytProperties.getGatewayUrl());
        httpTransport.setConnTimeout(String.valueOf(openAPIClientJytProperties.getConnTimeout()));
        httpTransport.setReadTimeout(String.valueOf(openAPIClientJytProperties.getReadTimeout()));
        return httpTransport;
    }

    /**
     * 金运通SDK-servlet注册
     *
     * @return
     */
    @Bean
    public ServletRegistrationBean getJytApiSDKServlet() {
        ServletRegistrationBean bean = new ServletRegistrationBean();
        ApiServiceClientServlet apiServiceClientServlet = new ApiServiceClientServlet();
        bean.setServlet(apiServiceClientServlet);
        bean.addInitParameter(ApiServiceClientServlet.NOTIFY_DISPATCHER_BEAN_NAME_KEY, "jytNotifyHandlerDispatcher");
        bean.addInitParameter(ApiServiceClientServlet.SUCCESS_RESPONSE_BODY_KEY, "");
        List<String> urlMappings = Lists.newArrayList();
        //网关异步通知地址
        urlMappings.add("/gateway/notify/jytNotify/" + JytServiceEnum.DEDUCT_PAY.getCode());//访问，可以添加多个
        bean.setUrlMappings(urlMappings);
        bean.setLoadOnStartup(3);
        return bean;
    }

//    @Bean("jytSftpFileHandler")
//    public JytSftpFileHandler jytSftpFileHandler() {
//        JytSftpFileHandler jytSftpFileHandler = new JytSftpFileHandler();
//        jytSftpFileHandler.setHost(openAPIClientJytProperties.getCheckfile().getHost());
//        jytSftpFileHandler.setPort(openAPIClientJytProperties.getCheckfile().getPort());
//        jytSftpFileHandler.setUsername(openAPIClientJytProperties.getCheckfile().getUsername());
//        jytSftpFileHandler.setPassword(openAPIClientJytProperties.getCheckfile().getPassword());
//        jytSftpFileHandler.setServerRoot(openAPIClientJytProperties.getCheckfile().getServerRoot());
//        jytSftpFileHandler.setLocalRoot(openAPIClientJytProperties.getCheckfile().getLocalRoot());
//        return jytSftpFileHandler;
//    }
}
