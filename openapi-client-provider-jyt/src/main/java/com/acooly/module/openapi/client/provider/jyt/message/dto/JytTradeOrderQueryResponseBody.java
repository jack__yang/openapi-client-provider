package com.acooly.module.openapi.client.provider.jyt.message.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author zhike 2018/5/8 17:57
 */
@Getter
@Setter
@XStreamAlias("body")
public class JytTradeOrderQueryResponseBody implements Serializable {

    /**
     * 支付状态
     当响应码为S0000000时有值。
     00支付成功
     01平台处理中（未验证支付）
     02银行处理中（已验证支付）
     03 支付失败
     */
    @NotBlank
    @XStreamAlias("tran_state")
    private String tranState;

    /**
     * 商户待支付订单号
     */
    @Size(max = 32)
    @XStreamAlias("order_id")
    @NotBlank
    private String orderId;

    /**
     * 交易时间
     商户发起交易的时间
     YYYYMMDDHHmmSS
     */
    @Size(max = 10)
    @XStreamAlias("mer_tran_time")
    @NotBlank
    private String merTranTime;

    /**
     * 交易金额
     * 支付金额(交易终态时，返回)
     */
    @Size(max = 32)
    @XStreamAlias("tran_amount")
    @NotBlank
    private String tranAmount;

    /**
     * 支付订单号
     * 可退款金额(交易终态时，返回)
     */
    @Size(max = 15)
    @XStreamAlias("avl_amount")
    @NotBlank
    private String avlAmount;

    /**
     * 备注信息
     * 结果说明
     */
    @Size(max = 60)
    @XStreamAlias("remark")
    private String remark;

    /**
     * 交易响应码
     */
    @Size(max = 8)
    @XStreamAlias("tran_resp_code")
    private String tranRespCode;

    /**
     * 交易响应描述
     */
    @Size(max = 50)
    @XStreamAlias("tran_resp_desc")
    private String tranRespDesc;
}
