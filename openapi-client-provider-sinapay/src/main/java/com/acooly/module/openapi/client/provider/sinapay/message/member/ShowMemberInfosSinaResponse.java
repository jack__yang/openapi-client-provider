package com.acooly.module.openapi.client.provider.sinapay.message.member;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayRedirect;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/7/10 15:41
 */
@Getter
@Setter
@SinapayApiMsg(service = SinapayServiceNameEnum.SHOW_MEMBER_INFOS_SINA, type = ApiMessageType.Response)
public class ShowMemberInfosSinaResponse extends SinapayRedirect {
}
