/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年5月6日
 *
 */
package com.acooly.module.openapi.client.provider.sinapay.message.trade;

import com.acooly.core.utils.Money;
import com.acooly.core.utils.validate.jsr303.MoneyConstraint;
import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayNotify;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * @author zhike
 */
public class TradeNotify extends SinapayNotify {
	/**
	 * 商户网站唯一订单号或者交易原始凭证号
	 *
	 * 商户网站唯一订单号或者交易原始凭证号
	 */
	@NotEmpty
	@Size(max = 32)
	@ApiItem(value = "outer_trade_no")
	private String outerTradeNo;

	/**
	 * 内部交易凭证号
	 *
	 * 新浪支付产生的交易凭证号
	 */
	@NotEmpty
	@Size(max = 32)
	@ApiItem(value = "inner_trade_no")
	private String innerTradeNo;

	/**
	 * 交易状态
	 *
	 * 交易状态， 详见附录中的交易状态
	 */
	@NotEmpty
	@Size(max = 20)
	@ApiItem(value = "trade_status")
	private String tradeStatus;

	/**
	 * 交易金额
	 *
	 * 单位元，可以含小数点
	 */
	@NotEmpty
	@MoneyConstraint
	@ApiItem(value = "trade_amount")
	private Money tradeAmount;

	/**
	 * 交易创建时间
	 *
	 * 交易创建时间，格式yyyyMMddHHmmss
	 */
	@NotEmpty
	@Size(max = 14)
	@ApiItem(value = "gmt_create")
	private String gmtCreate;

	/**
	 * 交易支付时间
	 *
	 * 交易支付时间，格式yyyyMMddHHmmss
	 */
	@Size(max = 14)
	@ApiItem(value = "gmt_payment")
	private String gmtPayment;

	/**
	 * 交易关闭时间
	 *
	 * 交易关闭时间，格式yyyyMMddHHmmss
	 */
	@Size(max = 14)
	@ApiItem(value = "gmt_close")
	private String gmtClose;

	/**
	 * 支付方式
	 *
	 * 用户实际支付使用的支付方式。格式：支付方式^金额^扩展|支付方式^金额^扩展。扩展信息内容以“，”分隔，针对不同支付方式的扩展定义见附录[6.
	 * 9支付方式扩展(异步通知)]。注意，商户之前传入的新浪支付回调地址（notify_url）需要包含特殊参数sinaPnsVersion=v0.1
	 */
	@Size(max = 1000)
	@ApiItem(value = "pay_method")
	private String payMethod;

	/**
	 * 代收完成金额
	 *
	 * 只有代收冻结交易且交易状态为TRADE_FINISHED时才返回该参数
	 */
	@MoneyConstraint(nullable = true)
	@ApiItem(value = "auth_finish_amount")
	private Money authFinishAmount;

	public String getOuterTradeNo() {
		return outerTradeNo;
	}

	public void setOuterTradeNo(String outerTradeNo) {
		this.outerTradeNo = outerTradeNo;
	}

	public String getInnerTradeNo() {
		return innerTradeNo;
	}

	public void setInnerTradeNo(String innerTradeNo) {
		this.innerTradeNo = innerTradeNo;
	}

	public String getTradeStatus() {
		return tradeStatus;
	}

	public void setTradeStatus(String tradeStatus) {
		this.tradeStatus = tradeStatus;
	}

	public Money getTradeAmount() {
		return tradeAmount;
	}

	public void setTradeAmount(Money tradeAmount) {
		this.tradeAmount = tradeAmount;
	}

	public String getGmtCreate() {
		return gmtCreate;
	}

	public void setGmtCreate(String gmtCreate) {
		this.gmtCreate = gmtCreate;
	}

	public String getGmtPayment() {
		return gmtPayment;
	}

	public void setGmtPayment(String gmtPayment) {
		this.gmtPayment = gmtPayment;
	}

	public String getGmtClose() {
		return gmtClose;
	}

	public void setGmtClose(String gmtClose) {
		this.gmtClose = gmtClose;
	}

	public String getPayMethod() {
		return payMethod;
	}

	public void setPayMethod(String payMethod) {
		this.payMethod = payMethod;
	}

	public Money getAuthFinishAmount() {
		return authFinishAmount;
	}

	public void setAuthFinishAmount(Money authFinishAmount) {
		this.authFinishAmount = authFinishAmount;
	}

}
