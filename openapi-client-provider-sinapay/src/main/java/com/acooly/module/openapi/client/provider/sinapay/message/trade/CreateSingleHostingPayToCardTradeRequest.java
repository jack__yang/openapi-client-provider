/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年12月12日
 *
 */
package com.acooly.module.openapi.client.provider.sinapay.message.trade;

import com.acooly.core.utils.Ids;
import com.acooly.core.utils.Money;
import com.acooly.core.utils.Strings;
import com.acooly.core.utils.validate.jsr303.MoneyConstraint;
import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.annotation.SinaGateWay;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayRequest;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayGateWayType;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayOutTradeCode;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import com.acooly.module.openapi.client.provider.sinapay.message.trade.dto.DebtChangeDetail;
import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;
import java.util.List;

/**
 * 3.14 创建单笔代付到提现卡交易
 * 
 * @author zhike
 */
@Getter
@Setter
@SinaGateWay(SinapayGateWayType.PAYMENT)
@SinapayApiMsg(service = SinapayServiceNameEnum.CREATE_SINGLE_HOSTING_PAY_TO_CARD_TRADE, type = ApiMessageType.Request)
public class CreateSingleHostingPayToCardTradeRequest extends SinapayRequest {
	/**
	 * 交易订单号
	 *
	 * 商户网站交易订单号，商户内部保证唯一
	 */
	@NotEmpty
	@Size(max = 32)
	@ApiItem(value = "out_trade_no")
	private String outTradeNo = Ids.oid();

	/**
	 * 交易码
	 *
	 * 商户网站代收交易业务码，见附录
	 */
	@NotEmpty
	@Size(max = 16)
	@ApiItem(value = "out_trade_code")
	private String outTradeCode = SinapayOutTradeCode.INVEST_OUT.code();

	/**
	 * 收款方式
	 */
	@NotEmpty
	@Size(max = 1000)
	@ApiItem(value = "collect_method")
	private String collectMethod;

	/** 绑卡ID (不序列化，用于组装collectMethod) */
	@Size(max = 32)
	@NotEmpty
	private String cardId;

	/**
	 * 收款人标识
	 *
	 * 商户系统用户ID、钱包系统会员ID
	 */
	@NotEmpty
	@Size(max = 50)
	private String payeeIdentityId;

	/**
	 * 收款人标识类型
	 *
	 * ID的类型，参考“标志类型”
	 */
	@NotEmpty
	@Size(max = 16)
	private String payeeIdentityType = "UID";

	/**
	 * 金额
	 *
	 * 金额
	 */
	@MoneyConstraint
	@ApiItem(value = "amount")
	private Money amount;

	/**
	 * 摘要
	 *
	 * 交易内容摘要
	 */
	@NotEmpty
	@Size(max = 64)
	@ApiItem(value = "summary")
	private String summary;

	/**
	 * 到账类型
	 * 
	 * (GENERAL： 普通FAST: 快速)
	 */
	@NotEmpty
	@Size(max = 16)
	@ApiItem(value = "payto_type")
	private String paytoType = "GENERAL";

	/**
	 * 商户标的号
	 *
	 * 对应“标的录入”接口中的标的号，用来关联此笔代收和标的
	 */
	@Size(max = 64)
	@ApiItem(value = "goods_id")
	private String goodsId;

	/**
	 * 用户IP/运营商IP
	 */
	@NotEmpty
	@Size(max = 32)
	@ApiItem(value = "user_ip")
	private String userIp;

	/**
	 * 债权变动明细列表
	 */
	@ApiItem(value = "creditor_info_list")
	private List<DebtChangeDetail> creditorInfoList = Lists.newArrayList();

	/**
	 * 交易关联号
	 */
	@NotEmpty
	@Size(max = 32)
	@ApiItem(value = "trade_related_no")
	private String tradeRelatedNo;


	public CreateSingleHostingPayToCardTradeRequest() {
		super();
	}

	/**
	 * @param payeeIdentityId
	 * @param amount
	 * @param summary
	 */
	public CreateSingleHostingPayToCardTradeRequest(String summary, String payeeIdentityId, String userIp, Money amount,
			String cardId) {
		this();
		this.payeeIdentityId = payeeIdentityId;
		this.amount = amount;
		this.summary = summary;
		this.userIp = userIp;
		this.cardId = cardId;
		initCollectMethod();
	}

	private void initCollectMethod() {
		if (Strings.isNotBlank(collectMethod)) {
			return;
		}
		StringBuilder sb = new StringBuilder();
		sb.append("binding_card^").append(getPayeeIdentityId()).append(",").append(getPayeeIdentityType()).append(",")
				.append(getCardId());
		setCollectMethod(sb.toString());
	}

}
