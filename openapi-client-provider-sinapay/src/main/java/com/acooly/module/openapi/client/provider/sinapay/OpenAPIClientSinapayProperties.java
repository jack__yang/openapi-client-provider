/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhike@acooly.cn 2017-09-16 16:11 创建
 */
package com.acooly.module.openapi.client.provider.sinapay;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import static com.acooly.module.openapi.client.provider.sinapay.OpenAPIClientSinapayProperties.PREFIX;


/**
 * 翼支付配置参数
 *
 * @author zhike@acooly.cn
 */
@Getter
@Setter
@ConfigurationProperties(prefix = PREFIX)
public class OpenAPIClientSinapayProperties {

    public static final String PREFIX = "acooly.openapi.client.sinapay";

    /**
     * 会员网关请求地址
     */
    private String memberGatewayUrl;

    /**
     * 收单网关请求地址
     */
    private String paymentGatewayUrl;


    /**
     * 下载网关地址
     */
    private String downloadUrl;

    /**
     * 商户号
     */
    private String partnerId;


    /**
     * 商户签名私钥
     */
    private String signPrivateKey;

    /**
     * 网关验签公钥
     */
    private String signPublicKey;

    /**
     * 网关加密公钥
     */
    private String encryptPublicKey;

    /**
     * 本系统域名（用于自动生成回调地址）
     */
    private String domain;

    /**
     * 本系统的通知Url或URL前缀
     */
    private String notifyUrl = "/openapi/client/shengpay/notify";

    /**
     * 连接超时时间（毫秒）
     */
    private long connTimeout = 10000;

    /**
     * 读超时时间（毫秒）
     */
    private long readTimeout = 30000;


    private Checkfile checkfile = new Checkfile();

    @Getter
    @Setter
    public static class Checkfile {

        private String host;
        private int port;
        private String username;
        private String password;

        private String serverRoot;
        private String localRoot;

    }


}
