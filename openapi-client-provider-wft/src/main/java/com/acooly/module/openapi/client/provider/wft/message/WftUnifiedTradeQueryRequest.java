package com.acooly.module.openapi.client.provider.wft.message;

import com.acooly.core.common.exception.OrderCheckException;
import com.acooly.core.common.facade.ResultCode;
import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.wft.domain.WftApiMsgInfo;
import com.acooly.module.openapi.client.provider.wft.domain.WftRequest;
import com.acooly.module.openapi.client.provider.wft.enums.WftServiceEnum;
import com.acooly.module.openapi.client.provider.wft.support.WftAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/**
 * @author zhike 2017/11/28 19:53
 */
@Getter
@Setter
@WftApiMsgInfo(service = WftServiceEnum.TRADE_ORDER_QUERY, type = ApiMessageType.Request)
public class WftUnifiedTradeQueryRequest extends WftRequest {

    /**
     * 商户订单号
     */
    @WftAlias(value = "out_trade_no")
    @Size(max = 32)
    private String outTradeNo;

    /**
     * 平台订单号
     */
    @WftAlias(value = "transaction_id")
    @Size(max = 32)
    private String transactionId;

    /**
     * 订单号
     */
    @WftAlias(value = "nonce_str")
    @NotBlank(message = "随机字符串不能为空")
    @Size(max = 32)
    private String nonceStr;

    @Override
    public void doCheck() {
        if (Strings.isBlank(outTradeNo) && Strings.isBlank(transactionId)) {
            throw new OrderCheckException(ResultCode.PARAMETER_ERROR.getCode(), "商户订单号和业务订单号不能同时为空");
        }
    }
}
