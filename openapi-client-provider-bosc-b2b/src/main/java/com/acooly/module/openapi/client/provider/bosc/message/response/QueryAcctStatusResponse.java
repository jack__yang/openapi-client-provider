package com.acooly.module.openapi.client.provider.bosc.message.response;

import com.acooly.module.openapi.client.provider.bosc.domain.BoscResponseDomain;
import com.acooly.module.openapi.client.provider.bosc.enums.ActivateStatusEnum;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QueryAcctStatusResponse extends BoscResponseDomain {

	/**
	 * 接收银行状态码（00000000：已激活 00000001：未激活）
	 */
	private String data;

	/**
	 * 接口返回状态标识
	 */
	private ActivateStatusEnum activateStatusEnum;

}
