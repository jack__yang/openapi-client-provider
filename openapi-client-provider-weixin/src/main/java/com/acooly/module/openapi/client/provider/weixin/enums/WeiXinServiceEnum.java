/*
 * ouwen@yiji.com Inc.
 * Copyright (c) 2017 All Rights Reserved.
 * create by ouwen
 * date:2017-03-30
 *
 */
package com.acooly.module.openapi.client.provider.weixin.enums;

import com.acooly.core.utils.enums.Messageable;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * inst_channel_api WeixinServiceEnum 枚举定义
 *
 * @author jiandao
 */
public enum WeixinServiceEnum implements Messageable {
  PAY_WEIXIN_UNIFIED_ORDER("pay.weixin.unified.order", "payWeixinUnifiedOrder", "微信统一下单支付"),
  ;

  private final String code;
  private final String key;
  private final String message;

  private WeixinServiceEnum(String code, String key, String message) {
    this.code = code;
    this.message = message;
    this.key = key;
  }

  public String getCode() {
    return code;
  }

  public String getMessage() {
    return message;
  }

  public String code() {
    return code;
  }

  public String message() {
    return message;
  }

  public String getKey() {
    return key;
  }

  public static Map<String, String> mapping() {
    Map<String, String> map = new LinkedHashMap<String, String>();
    for (WeixinServiceEnum type : values()) {
      map.put(type.getCode(), type.getMessage());
    }
    return map;
  }

  /**
   * 通过枚举值码查找枚举值。
   *
   * @param code 查找枚举值的枚举值码。
   * @return 枚举值码对应的枚举值。
   * @throws IllegalArgumentException 如果 code 没有对应的 Status 。
   */
  public static WeixinServiceEnum find(String code) {
    for (WeixinServiceEnum status : values()) {
      if (status.getCode().equals(code)) {
        return status;
      }
    }
    return null;
  }

  /**
   * 通过枚举值码查找枚举值。
   *
   * @param key 查找枚举值的枚举值码。
   * @return 枚举值码对应的枚举值。
   * @throws IllegalArgumentException 如果 key 没有对应的 Status 。
   */
  public static WeixinServiceEnum findByKey(String key) {
    for (WeixinServiceEnum status : values()) {
      if (status.getKey().equals(key)) {
        return status;
      }
    }
    return null;
  }

  /**
   * 获取全部枚举值。
   *
   * @return 全部枚举值。
   */
  public static List<WeixinServiceEnum> getAll() {
    List<WeixinServiceEnum> list = new ArrayList<WeixinServiceEnum>();
    for (WeixinServiceEnum status : values()) {
      list.add(status);
    }
    return list;
  }

  /**
   * 获取全部枚举值码。
   *
   * @return 全部枚举值码。
   */
  public static List<String> getAllCode() {
    List<String> list = new ArrayList<String>();
    for (WeixinServiceEnum status : values()) {
      list.add(status.code());
    }
    return list;
  }
}
