package com.acooly.module.openapi.client.provider.hx.message.xStream.netBankPayQuery.response;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import lombok.Data;

/**
 * @author fufeng 2018/3/2 15:50.
 */
@Data
@XmlAccessorType(XmlAccessType.FIELD)
@XStreamAlias("body")
public class RespNetBankQueryBody {

    /**
     *商户订单号
     */
    @XStreamAlias("MerBillNo")
    private String merBillNo;

    /**
     * IPS 订单号
     */
    @XStreamAlias("IpsBillNo")
    private String ipsBillNo;

    /**
     * 交易类型
     */
    @XStreamAlias("TradeType")
    private String tradeType;

    /**
     *交易币种
     */
    @XStreamAlias("Currency")
    private String currency;

    /**
     *交易金额
     */
    @XStreamAlias("Amount")
    private String amount;

    /**
     *商户订单日期
     * yyyyMMdd
     */
    @XStreamAlias("MerBillDate")
    private String merBillDate;

    /**
     *IPS 订单时间
     * yyyyMMddHHmmss
     */
    @XStreamAlias("IpsBillTime")
    private String ipsBillTime;

    /**
     *订单备注信息
     */
    @XStreamAlias("Attach")
    private String attach;

    /**
     *订单状态
     * P：处理中 N：失败 Y：成功
     */
    @XStreamAlias("Status")
    private String status;

    /**
     *订单已退金额
     * 当请求字段 Version 为 v1.0.1 时
     返回该字段
     */
    @XStreamAlias("RefundAmount")
    private String refundAmount;

    /**
     *订单已拒付金额
     * 当请求字段 Version 为 v1.0.1 时
     返回该字段
     */
    @XStreamAlias("RefuseAmout")
    private String refuseAmout;

}
