package com.acooly.module.openapi.client.provider.hx.message.xStream.common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import lombok.Data;

@Data
@XmlAccessorType(XmlAccessType.FIELD)
@XStreamAlias("Head")
public class ReqWithdrawHead {
    /**
     *固定值：“ v1.0.0”
     */
    @XStreamAlias("Version")
    private String version;
    /**
     *商户号，IPS 给商户分配的唯一标识号
     */
    @XStreamAlias("MerCode")
    private String merCode;
    /**
     *商户名
     */
    @XStreamAlias("MerName")
    private String merName;
    /**
     *交易账户号
     */
    @XStreamAlias("Account")
    private String account;
    /**
     *消息唯一标示，交易必输，查询可选
     */
    @XStreamAlias("MsgId")
    private String msgId;
    /**
     *格式：yyyyMMddHHmmss，例如：20160913120000
     */
    @XStreamAlias("ReqDate")
    private String reqDate;
    /**
     *MD5 签名
     *Body 节点不为空 对<body>……</body>节点字符串
     *+商户 MD5 证书进行签名（包括 body 标签）；
     *body 节点为空时 使用商户证书的进行签名
     *注意：+号在此处代表字符串连接。
     */
    @XStreamAlias("Signature")
    private String signature = "0";
}
