package com.acooly.module.openapi.client.provider.alipay;

import java.util.Map;

import org.apache.log4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.acooly.module.openapi.client.api.AbstractApiServiceClient;
import com.acooly.module.openapi.client.api.exception.ApiClientException;
import com.acooly.module.openapi.client.api.marshal.ApiMarshal;
import com.acooly.module.openapi.client.api.marshal.ApiUnmarshal;
import com.acooly.module.openapi.client.api.message.PostRedirect;
import com.acooly.module.openapi.client.api.transport.Transport;
import com.acooly.module.openapi.client.provider.alipay.domain.AlipayNotify;
import com.acooly.module.openapi.client.provider.alipay.domain.AlipayRequest;
import com.acooly.module.openapi.client.provider.alipay.domain.AlipayResponse;
import com.acooly.module.openapi.client.provider.alipay.marshall.AlipayNotifyUnmarshall;

import lombok.extern.slf4j.Slf4j;

/**
 * ApiService执行器
 *
 * @author zhangpu
 */
@Component("alipayApiServiceClient")
@Slf4j
public class AlipayApiServiceClient //implements ApiServiceClient<AlipayRequest, AlipayResponse, AlipayNotify, AlipayNotify>{
        extends AbstractApiServiceClient<AlipayRequest, AlipayResponse, AlipayNotify, AlipayNotify> {


	public static final String PROVIDER_NAME = "alipay";
	
	@Autowired
	private OpenAPIClientAlipayProperties openAPIClientAlipayProperties;
	
	@Autowired
    private AlipayNotifyUnmarshall notifyUnmarshal;
	
	@Override
	public AlipayResponse execute(AlipayRequest request) {
		return null;
	}


	@Override
	public String redirectGet(AlipayRequest request) {
		return null;
	}


	@Override
	public PostRedirect redirectPost(AlipayRequest request) {
		return null;
	}


	@Override
	public AlipayNotify notice(Map<String, String> notifyData, String serviceKey) {
		try {
			AlipayNotify notify = getNoticeUnmarshal().unmarshal(notifyData, serviceKey);
            afterNotice(notify);
			return notify;
		} catch (ApiClientException e) {
			log.warn("通知验签客户端:{}",e.getMessage());
			throw new ApiClientException("客户端:" + e.getMessage());
		} catch (Exception e) {
			log.error("通知验签内部错误",e);
			throw new ApiClientException("内部错误:" + e.getMessage());
		}
	}


	@Override
	public AlipayNotify result(Map<String, String> notifyData, String serviceKey) {
		return null;
	}

	protected void beforeExecute(AlipayRequest request) {
		MDC.put("service", request.getService());
	}

	@Override
	public String getName() {
		return PROVIDER_NAME;
	}


	@Override
	protected ApiMarshal<String, AlipayRequest> getRequestMarshal() {
		return null;
	}


	@Override
	protected ApiUnmarshal<AlipayResponse, String> getResponseUnmarshal() {
		return null;
	}


	@Override
	protected ApiMarshal<String, AlipayRequest> getRedirectMarshal() {
		return null;
	}


	@Override
	protected ApiUnmarshal<AlipayNotify, Map<String, String>> getNoticeUnmarshal() {
		return notifyUnmarshal;
	}


	@Override
	protected ApiUnmarshal<AlipayNotify, Map<String, String>> getReturnUnmarshal() {
		return null;
	}


	@Override
	protected Transport getTransport() {
		return null;
	}
}
