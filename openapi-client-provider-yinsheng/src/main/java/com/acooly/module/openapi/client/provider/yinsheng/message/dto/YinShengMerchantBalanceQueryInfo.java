package com.acooly.module.openapi.client.provider.yinsheng.message.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/2/22 9:55
 */
@Getter
@Setter
public class YinShengMerchantBalanceQueryInfo extends YinShengInfo{

    /**
     * 商户号
     * 1.当商户号为空时，默认将partner_id,作为商户号
     * 2.如果提现的商户号和合作商户号不一致则需要检查机构信息
     */
    private String merchant_usercode;
}
