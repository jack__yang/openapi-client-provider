package com.acooly.module.openapi.client.provider.yinsheng.message.dto;

import com.acooly.module.openapi.client.provider.yinsheng.enums.YinShengScanPayBankTypeEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/1/8 16:59
 */
@Getter
@Setter
public class YinShengScanPayInfo extends YinShengInfo {

    /**
     * 默认为支付宝扫码支付
     * 银行类型 微信-1902000|支付宝-1903000|QQ扫码-1904000
     */
    private String bank_type = YinShengScanPayBankTypeEnum.ALI_SCAN_PAY.getCode();

}
