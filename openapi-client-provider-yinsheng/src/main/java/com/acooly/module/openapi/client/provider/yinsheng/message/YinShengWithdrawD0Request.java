package com.acooly.module.openapi.client.provider.yinsheng.message;

import com.acooly.core.common.exception.OrderCheckException;
import com.acooly.core.common.facade.ResultCode;
import com.acooly.core.utils.validate.Validators;
import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.yinsheng.domain.YinShengApiMsgInfo;
import com.acooly.module.openapi.client.provider.yinsheng.domain.YinShengRequest;
import com.acooly.module.openapi.client.provider.yinsheng.enums.YinShengServiceEnum;
import com.acooly.module.openapi.client.provider.yinsheng.message.dto.YinShengWithdrawD0Info;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

/**
 * @author zhike 2018/2/8 13:59
 */
@Getter
@Setter
@YinShengApiMsgInfo(service = YinShengServiceEnum.WITHDRAW_D0, type = ApiMessageType.Request)
public class YinShengWithdrawD0Request extends YinShengRequest {

    /**
     * 通知地址
     */
    @NotBlank
    private String notify_url;

    /**
     * 提现申请业务信息
     */
    @ApiItem(value = "biz_content")
    private YinShengWithdrawD0Info yinShengWithdrawD0Info;

    @Override
    public void doCheck() {
        super.doCheck();
        if (yinShengWithdrawD0Info == null) {
            throw new OrderCheckException(ResultCode.PARAMETER_ERROR.getCode(), "D0提现结算申请业务信息不能为空");
        }
        Validators.assertJSR303(yinShengWithdrawD0Info);
    }
}
