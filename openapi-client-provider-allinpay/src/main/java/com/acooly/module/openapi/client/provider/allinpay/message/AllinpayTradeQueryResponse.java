package com.acooly.module.openapi.client.provider.allinpay.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.allinpay.domain.AllinpayApiMsgInfo;
import com.acooly.module.openapi.client.provider.allinpay.domain.AllinpayResponse;
import com.acooly.module.openapi.client.provider.allinpay.enums.AllinpayServiceEnum;
import com.acooly.module.openapi.client.provider.allinpay.message.dto.AllinpayTradeQueryResponseBody;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

/**
 * @Auther: zhike
 * @Date: 2018/8/29 14:02
 * @Description:
 */
@Getter
@Setter
@AllinpayApiMsgInfo(service = AllinpayServiceEnum.TRADE_QUERY,type = ApiMessageType.Response)
@XStreamAlias("AIPG")
public class AllinpayTradeQueryResponse extends AllinpayResponse {
    /**
     * 响应业务实体
     */
    @XStreamAlias("QTRANSRSP")
    private AllinpayTradeQueryResponseBody responseBody;
}
