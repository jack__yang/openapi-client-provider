package com.acooly.openapi.client.provider.yipay;

import com.acooly.core.common.BootApp;
import com.acooly.core.utils.Dates;
import com.acooly.core.utils.Ids;
import com.acooly.module.openapi.client.provider.fudian.FudianApiService;
import com.acooly.module.openapi.client.provider.fudian.OpenAPIClientFudianProperties;
import com.acooly.module.openapi.client.provider.fudian.message.fund.*;
import com.acooly.module.openapi.client.provider.fudian.message.fund.dto.LoanCallbackInvestInfo;
import com.acooly.module.openapi.client.provider.fudian.message.member.*;
import com.acooly.module.openapi.client.provider.fudian.message.other.MigrationDataRequest;
import com.acooly.module.openapi.client.provider.fudian.message.other.MigrationDataResponse;
import com.acooly.module.openapi.client.provider.fudian.message.query.*;
import com.acooly.module.openapi.client.provider.yipay.YipayApiService;
import com.acooly.module.openapi.client.provider.yipay.domain.YipayResponse;
import com.acooly.module.openapi.client.provider.yipay.enums.YipayAreaCodeEnum;
import com.acooly.module.openapi.client.provider.yipay.message.*;
import com.acooly.openapi.client.fudian.support.IdCardGenerator;
import com.acooly.openapi.client.provider.fudian.FudianAbstractTest;
import com.alibaba.fastjson.JSON;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.List;

/**
 * @author zhangpu@yiji.com
 */
@SpringBootApplication
@BootApp(sysName = "test")
public class YipayTest extends FudianAbstractTest {

    @Autowired
    private YipayApiService yipayApiService;

    @Autowired
    private OpenAPIClientFudianProperties openAPIClientFudianProperties;

    /**
     * 测试用户注册
     */
    @Test
    public void testThreeElementsVerify(){
        YipayThreeElementsVerifyRequest request = new YipayThreeElementsVerifyRequest();
        request.setRealName("中国工商银行测试卡2号");
        request.setBankCardNo("624200333333333335");
        request.setCertNo("440582199005162696");
        request.setReqIp("127.0.0.1");
        YipayThreeElementsVerifyResponse response = yipayApiService.threeElementsVerify(request);
        System.out.println("三要素验证响应报文："+response);

    }


    /**
     * 测试用户注册
     */
    @Test
    public void testFourElementsVerify(){
        YipayFourElementsVerifyRequest request = new YipayFourElementsVerifyRequest();
        request.setRealName("中国工商银行测试卡2号");
        request.setBankCardNo("624200333333333335");
        request.setCertNo("440582199005162696");
        request.setReqIp("127.0.0.1");
        request.setMobile("13660713452");
        YipayFourElementsVerifyResponse response = yipayApiService.fourElementsVerify(request);
        System.out.println("四要素验证响应报文："+response);
    }

    /**
     * 测试全网运营商三要素验证
     */
    @Test
    public void testOperatorThreeElmsValid(){
        YipayOperatorThreeElmsValidRequest request = new YipayOperatorThreeElmsValidRequest();
        request.setRealName("中国工商银行测试卡2号");
        request.setCertNo("440582199005162696");
        request.setReqIp("127.0.0.1");
        request.setMobile("13660713452");
        YipayOperatorThreeElmsValidResponse response = yipayApiService.operatorThreeElmsValid(request);
        System.out.println("全网运营商三要素验证响应报文："+response);
    }

    /**
     * 全网实时位置验证接口
     */
    @Test
    public void testRealTimeLocationVerifyPosition(){
        YipayRealTimeLocationVerifyPositionRequest request = new YipayRealTimeLocationVerifyPositionRequest();
        request.setReqIp("127.0.0.1");
        request.setMobile("18696725229");
        request.setLatitude("106.725651");
        request.setLongitude("30.03892");
        YipayRealTimeLocationVerifyPositionResponse response = yipayApiService.realTimeLocationVerifyPosition(request);
        System.out.println("全网实时位置验证接口响应报文："+response);
    }

    /**
     * 运营商实时城市位置核验接口
     */
    @Test
    public void testCityLocationVerifyLocation(){
        YipayCityLocationVerifyLocationRequest request = new YipayCityLocationVerifyLocationRequest();
        request.setReqIp("127.0.0.1");
        request.setMobile("18696725229");
        request.setAreaCode(YipayAreaCodeEnum.AREA_CODE_510100.getCode());
        YipayCityLocationVerifyLocationResponse response = yipayApiService.cityLocationVerifyLocation(request);
        System.out.println("运营商实时城市位置核验接口响应报文："+response);
    }

    /**
     * 运营商在网状态查询
     */
    @Test
    public void testPhoneInfoValidOnlineStatus(){
        YipayPhoneInfoValidOnlineStatusRequest request = new YipayPhoneInfoValidOnlineStatusRequest();
        request.setReqIp("127.0.0.1");
        request.setMobile("18696725229");
        YipayPhoneInfoValidOnlineStatusResponse response = yipayApiService.phoneInfoValidOnlineStatus(request);
        System.out.println("运营商在网状态查询响应报文："+response);
    }

    /**
     * 运营商入网时长查询
     */
    @Test
    public void testWholeNetworkMobileTimeQuery(){
        YipayWholeNetworkMobileTimeQueryRequest request = new YipayWholeNetworkMobileTimeQueryRequest();
        request.setReqIp("127.0.0.1");
        request.setMobile("18696725229");
        YipayWholeNetworkMobileTimeQueryResponse response = yipayApiService.wholeNetworkMobileTimeQuery(request);
        System.out.println("运营商入网时长查询响应报文："+response);
    }

    /**
     * 电信运营商二要素验证(手机号+身份证)
     */
    @Test
    public void testTwoElemValidById(){
        YipayTwoElemValidByIdRequest request = new YipayTwoElemValidByIdRequest();
        request.setReqIp("127.0.0.1");
        request.setMobile("18696725229");
        request.setIdCardNo("500221198810192313");
        YipayTwoElemValidByIdResponse response = yipayApiService.twoElemValidById(request);
        System.out.println("电信运营商二要素验证(手机号+身份证)响应报文："+response);
    }

    /**
     * 身份证二要素验证接口
     */
    @Test
    public void testCertTwoElementsVerify(){
        YipayCertTwoElementsVerifyRequest request = new YipayCertTwoElementsVerifyRequest();
        request.setReqIp("127.0.0.1");
        request.setCertNo("500221198810192121");
        request.setCertType("00");
        request.setRealName("志客");
        YipayCertTwoElementsVerifyResponse response = yipayApiService.certTwoElementsVerify(request);
        System.out.println("身份证二要素验证接口响应报文："+response);
    }

    /**
     * 异步实时代收接口
     */
    @Test
    public void testDeduct(){
        YipayDeductRequest request = new YipayDeductRequest();
        request.setReqIp("127.0.0.1");
        request.setCertNo("500221198810192313");
        request.setRealName("韦崇凯");
        request.setBankCardNo("6217711201085972");
        request.setAmount("1");
        request.setBranchName("中信银行");
        request.setBankCode("867400");
        request.setMobile("18696725229");
        request.setTrsSummary("测试");
        request.setTradeOrderNo(Ids.oid());
        YipayDeductResponse response = yipayApiService.deduct(request);
        System.out.println("异步实时代收接口响应报文："+response);
    }

    /**
     * 交易查询接口（根据商户号来查询）
     */
    @Test
    public void testTradeQuery(){
        YipayTradeQueryRequest request = new YipayTradeQueryRequest();
        request.setReqIp("127.0.0.1");
        request.setTradeOrderNo("o18051709522413400001");
        request.setOrderType("TRADE");
        YipayTradeQueryResponse response = yipayApiService.tradeQuery(request);
        System.out.println("交易查询响应报文："+response);
    }

//    @Test
//    public void testLoanCallback() {
//        LoanCallbackRequest request = new LoanCallbackRequest();
//        List<LoanCallbackInvestInfo> investInfos = Lists.newArrayList();
//        for(int i=0;i<2;i++) {
//            LoanCallbackInvestInfo investInfo = new LoanCallbackInvestInfo();
//            investInfo.setCapital("323"+i);
//            investInfo.setInterest("dsd");
//            investInfo.setInterestFee("20");
//            investInfos.add(investInfo);
//        }
//        request.setInvestList(investInfos);
//        request.setLoanTxNo("23234433");
//        LoanCallbackResponse response = yipayApiService.loanCallback(request);
//        System.out.println("提现退汇查询响应报文："+ JSON.toJSONString(response));
//    }

}
