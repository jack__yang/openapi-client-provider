/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-11-15 22:57 创建
 */
package com.acooly.openapi.client.provider.bosc.testjson;

import com.acooly.module.openapi.client.api.util.Jsons;
import com.alibaba.fastjson.JSON;

/**
 * @author zhangpu 2017-11-15 22:57
 */
public class JsonParser {


    public static void main(String[] args) {

        // marshall
        Group group = new Group(1l, "acooly");
        group.getUsers().add(new User(1l, "zhangpu"));
        group.getUsers().add(new User(2l, "qiubo"));
//        group.getUsers().add(new User(3l, "liuyuxiang"));
//        group.getUsers().add(new User(4l,"cuifuqiang"));


        String json = JSON.toJSONString(group);
        System.out.println("json: " + json);

        Group newGroup = Jsons.parse(json, Group.class);
        System.out.println(newGroup);

    }

}
