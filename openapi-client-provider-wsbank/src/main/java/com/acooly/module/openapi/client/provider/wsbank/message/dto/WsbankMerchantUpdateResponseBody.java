package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.acooly.module.openapi.client.provider.wsbank.message.base.WsbankResponseInfo;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author zhike 2018/5/22 15:32
 */
@Getter
@Setter
@XStreamAlias("body")
public class WsbankMerchantUpdateResponseBody  implements Serializable  {

    /**
     * 返回码组件。当ResultStatus=S时才有后续的参数返回。
     */
    @XStreamAlias("RespInfo")
    private WsbankResponseInfo responseInfo;
    
	/**
	 * 外部交易号。合作方系统生成的外部交易号，同一交易号被视为同一笔交易。
	 */
    @Size(max = 64)
	@NotBlank
	@XStreamAlias("OutTradeNo")
	private String outTradeNo;
}
