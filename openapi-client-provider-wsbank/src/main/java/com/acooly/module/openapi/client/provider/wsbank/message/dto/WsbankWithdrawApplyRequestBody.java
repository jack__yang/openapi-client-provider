package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.io.Serializable;

@Getter
@Setter
@XStreamAlias("body")
public class WsbankWithdrawApplyRequestBody implements Serializable {
	
	private static final long serialVersionUID = -7751454403626363795L;

	/**
     * 合作方机构号（网商银行分配）
     */
    @Size(max = 32)
    @XStreamAlias("IsvOrgId")
    @NotBlank
    private String isvOrgId;

    /**
     * 提现商户号
     */
    @Size(max = 32)
    @XStreamAlias("MerchantId")
    @NotBlank
    private String merchantId;

    /**
     * 外部交易号。合作方系统生成的外部交易号，同一交易号被视为同一笔交易
     */
    @Size(max = 64)
    @XStreamAlias("OutTradeNo")
    @NotBlank
    private String outTradeNo;

    /**
     * 提现总金额
     */
    @XStreamAlias("TotalAmount")
    private String totalAmount;

    /**
     * 币种
     */
    @Size(max = 32)
    @XStreamAlias("Currency")
    @NotBlank
    private String currency = "CNY";

    /**
     * 平台设置提现手续费用（银行控制盖帽比率），用户实收资金= TotalAmount - PlatformFee
     */
    @Size(max = 32)
    @XStreamAlias("PlatformFee")
    private String platformFee;

    /**
     * 币种
     */
    @Size(max = 3)
    @XStreamAlias("FeeCurrency")
    @NotBlank
    private String feeCurrency = "CNY";

    /**
     * 备注
     */
    @Size(max = 32)
    @XStreamAlias("Memo")
    private String memo;
}
