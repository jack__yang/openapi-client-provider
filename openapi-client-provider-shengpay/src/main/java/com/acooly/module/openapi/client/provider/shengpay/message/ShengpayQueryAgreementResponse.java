package com.acooly.module.openapi.client.provider.shengpay.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.shengpay.domain.ShengpayApiMsg;
import com.acooly.module.openapi.client.provider.shengpay.domain.ShengpayResponse;
import com.acooly.module.openapi.client.provider.shengpay.enums.ShengpayServiceNameEnum;
import com.acooly.module.openapi.client.provider.shengpay.message.dto.ShengpayAgreementInfo;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author zhike 2018/5/17 19:00
 */
@Getter
@Setter
@ShengpayApiMsg(service = ShengpayServiceNameEnum.QUERY_AGREEMENT,type = ApiMessageType.Response)
public class ShengpayQueryAgreementResponse extends ShengpayResponse {

    /**
     * 协议列表
     */
    private List<ShengpayAgreementInfo> agreementInfoList;
}
