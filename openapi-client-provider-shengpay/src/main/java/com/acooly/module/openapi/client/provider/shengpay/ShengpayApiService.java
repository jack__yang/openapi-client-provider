/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhike@acooly.cn 2017-09-25 14:40 创建
 */
package com.acooly.module.openapi.client.provider.shengpay;

import com.acooly.module.openapi.client.provider.shengpay.domain.ShengpayNotify;
import com.acooly.module.openapi.client.provider.shengpay.enums.ShengpayServiceNameEnum;
import com.acooly.module.openapi.client.provider.shengpay.message.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @author zhike 2017-09-25 14:40
 */
@Component
public class ShengpayApiService {

    @Resource(name = "shengpayApiServiceClient")
    private ShengpayApiServiceClient shengpayApiServiceClient;

    @Autowired
    private OpenAPIClientShengpayProperties properties;

    /**
     * 创建订单
     * @param request
     * @return
     */
    public ShengpayCreatePaymentOrderResponse createPaymentOrder(ShengpayCreatePaymentOrderRequest request) {
        request.setNotifyUrl(properties.getDomain()+ShengpayConstants.NOTIFY_SUB_URL+ShengpayServiceNameEnum.PAYMENT_CONFIRM.getCode());
        ShengpayCreatePaymentOrderResponse response = (ShengpayCreatePaymentOrderResponse)shengpayApiServiceClient.execute(request);
        return response;
    }

    /**
     * 签约预校验
     * @param request
     * @return
     */
    public ShengpayPrecheckForSignResponse precheckForSign(ShengpayPrecheckForSignRequest request) {
        ShengpayPrecheckForSignResponse response = (ShengpayPrecheckForSignResponse)shengpayApiServiceClient.execute(request);
        return response;
    }

    /**
     * 签约确认
     * @param request
     * @return
     */
    public ShengpaySignConfirmResponse signConfirm(ShengpaySignConfirmRequest request) {
        ShengpaySignConfirmResponse response = (ShengpaySignConfirmResponse)shengpayApiServiceClient.execute(request);
        return response;
    }

    /**
     * 支付预校验
     * @param request
     * @return
     */
    public ShengpayPrecheckForPaymentResponse precheckForPayment(ShengpayPrecheckForPaymentRequest request) {
        ShengpayPrecheckForPaymentResponse response = (ShengpayPrecheckForPaymentResponse)shengpayApiServiceClient.execute(request);
        return response;
    }

    /**
     * 支付确认
     * @param request
     * @return
     */
    public ShengpayPaymentConfirmResponse paymentConfirm(ShengpayPaymentConfirmRequest request) {
        ShengpayPaymentConfirmResponse response = (ShengpayPaymentConfirmResponse)shengpayApiServiceClient.execute(request);
        return response;
    }

    /**
     * 协议查询
     * @param request
     * @return
     */
    public ShengpayQueryAgreementResponse queryAgreement(ShengpayQueryAgreementRequest request) {
        ShengpayQueryAgreementResponse response = (ShengpayQueryAgreementResponse)shengpayApiServiceClient.execute(request);
        return response;
    }

    /**
     * 解约
     * @param request
     * @return
     */
    public ShengpayUnsignResponse unsign(ShengpayUnsignRequest request) {
        ShengpayUnsignResponse response = (ShengpayUnsignResponse)shengpayApiServiceClient.execute(request);
        return response;
    }

    /**
     * 单笔订单查询
     * @param request
     * @return
     */
    public ShengpaySingleOrderQueryResponse singleOrderQuery(ShengpaySingleOrderQueryRequest request) {
        request.setSenderId(properties.getMerchantNo());
        request.setMerchantNo(properties.getMerchantNo());
        ShengpaySingleOrderQueryResponse response = shengpayApiServiceClient.executeQuery(request);
        return response;
    }
    /**
     * 同步，异步通知转化
     * serviceKey :YipayServiceNameEnum枚举中取对应的code
     * @param request 请求
     * @param serviceKey 服务标识
     * @return
     */
    public ShengpayNotify notice(HttpServletRequest request, String serviceKey) {
        return  shengpayApiServiceClient.notice(request, serviceKey);
    }
}
