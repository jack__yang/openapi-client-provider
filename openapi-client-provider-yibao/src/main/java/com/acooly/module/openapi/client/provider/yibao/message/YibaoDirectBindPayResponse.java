package com.acooly.module.openapi.client.provider.yibao.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.yibao.domain.YibaoAlias;
import com.acooly.module.openapi.client.provider.yibao.domain.YibaoApiMsg;
import com.acooly.module.openapi.client.provider.yibao.domain.YibaoResponse;
import com.acooly.module.openapi.client.provider.yibao.enums.YibaoServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/**
 * @author zhike 2018/6/26 14:37
 */
@Getter
@Setter
@YibaoApiMsg(service = YibaoServiceNameEnum.YIBAO_BINDPAY_DIRECT,type = ApiMessageType.Response)
public class YibaoDirectBindPayResponse extends YibaoResponse {

    /**
     * 银行流水号
     */
    @YibaoAlias(value = "yborderid")
    private String bankOrderNo;

    /**
     * 计费商编
     */
    @Size(max = 32)
    @YibaoAlias(value = "csmerchantno")
    private String csMerchantNo;

    /**
     * 单位：元
     * 精确到两位小数
     * 大于等于 0.01
     */
    @NotBlank
    @Size(max = 16)
    @YibaoAlias(value = "amount")
    private String amount;

    /**
     * 订单状态
     * PROCESSING：处理中
     * PAY_FAIL：支付失败
     * TIME_OUT：超时失败
     * FAIL：系统异常
     * （FAIL 是非终态是异常状态，出现此状态建议查询）
     */
    @YibaoAlias(value = "status")
    private String status;

    /**
     * 扩展字段
     */
    @Size(max = 42)
    @YibaoAlias(value = "free1")
    private String freeOne;

    /**
     * 扩展字段
     */
    @Size(max = 42)
    @YibaoAlias(value = "free2")
    private String freeTwo;

    /**
     * 扩展字段
     */
    @Size(max = 42)
    @YibaoAlias(value = "free3")
    private String freeThree;
}
