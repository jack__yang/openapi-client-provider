package com.acooly.module.openapi.client.provider.wewallet.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.wewallet.domain.WeWalletApiMsgInfo;
import com.acooly.module.openapi.client.provider.wewallet.domain.WeWalletRequest;
import com.acooly.module.openapi.client.provider.wewallet.enums.WeWalletServiceEnum;

import lombok.Getter;
import lombok.Setter;

/**
 * @author fufeng
 */
@Getter
@Setter
@WeWalletApiMsgInfo(service = WeWalletServiceEnum.WEWALLET_TICKET, type = ApiMessageType.Request)
public class WeWalletTicketRequest extends WeWalletRequest {

    /**
     * 微众分配平台标识
     */
    private String appId;
    /**
     * app_id 的访问令牌
     */
    private String accessToken;
    /**
     * ticket 类型
     */
    private String type;
    /**
     * 版本号，默认值：1.0.0
     */
    private String version;
    /**
     * 合作方用户唯一标识，申请
     NONCE Ticket 时为必填
     */
    private String userId;

}
