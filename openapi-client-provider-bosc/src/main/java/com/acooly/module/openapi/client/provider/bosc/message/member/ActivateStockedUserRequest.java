package com.acooly.module.openapi.client.provider.bosc.message.member;

import com.acooly.module.openapi.client.provider.bosc.domain.ApiMsgInfo;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscRequest;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

@ApiMsgInfo(service = BoscServiceNameEnum.ACTIVATE_STOCKED_USER, type = ApiMessageType.Request)
public class ActivateStockedUserRequest extends BoscRequest {
	/**
	 * 请求流水号
	 */
	@NotEmpty
	@Size(max = 50)
	private String requestNo;
	/**
	 * 页面回跳 URL
	 */
	@NotEmpty
	@Size(max = 100)
	private String redirectUrl;
	/**
	 * 平台用户编号
	 */
	@NotEmpty
	@Size(max = 50)
	private String platformUserNo;
	/**
	 * 见【用户授权列表】；此处可传多个值，传多个值用“,”英文半角逗号分隔
	 */
	@Size(max = 100)
	private String authList;
	/**
	 * 鉴权验证类型，默认填 LIMIT（强制四要素），即四要素完全通过（姓名、身份证 号、银行卡号，银行预留手机号） 方可激活成功
	 */
	private String checkType = "LIMIT";
	
	public ActivateStockedUserRequest () {
		setService (BoscServiceNameEnum.ACTIVATE_STOCKED_USER.code ());
	}
	
	public ActivateStockedUserRequest (String requestNo, String redirectUrl, String platformUserNo) {
		this();
		this.requestNo = requestNo;
		this.redirectUrl = redirectUrl;
		this.platformUserNo = platformUserNo;
	}
	
	public String getRequestNo () {
		return requestNo;
	}
	
	public void setRequestNo (String requestNo) {
		this.requestNo = requestNo;
	}
	
	public String getRedirectUrl () {
		return redirectUrl;
	}
	
	public void setRedirectUrl (String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}
	
	public String getPlatformUserNo () {
		return platformUserNo;
	}
	
	public void setPlatformUserNo (String platformUserNo) {
		this.platformUserNo = platformUserNo;
	}
	
	public String getAuthList () {
		return authList;
	}
	
	public void setAuthList (String authList) {
		this.authList = authList;
	}
	
	public String getCheckType () {
		return checkType;
	}
	
	public void setCheckType (String checkType) {
		this.checkType = checkType;
	}
}