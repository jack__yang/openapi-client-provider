package com.acooly.module.openapi.client.provider.bosc.message.fund;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.bosc.domain.ApiMsgInfo;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscResponse;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;

@ApiMsgInfo(service = BoscServiceNameEnum.CONFIRM_CHECKFILE, type = ApiMessageType.Response)
public class ConfirmCheckFileResponse extends BoscResponse {

}